import struct

import binascii

from file_forensics.util import ms_time_to_unix


class DictReprMixin:
    def __str__(self):
        return self.__dict__.__str__()
    __repr__ = __str__


class LnkParsingException(Exception):
    def __init__(self, message):
        self.message = message


class FlagAttributes(DictReprMixin):
    flags = {}

    def __init__(self, data):
        self._set_flag_attrs(data)

    def _set_flag_attrs(self, flag_data):
        """
        This is add attributes to the instance of this class for each key in the flag table passed based on the flag
        data.

        :param flag_data: a data of type immutable byte array (bytes)
        """
        for key in self.flags:
            index = self.flags[key][0]
            flag = self.flags[key][1]
            setattr(self, key, bool(flag_data[index] & flag))

    @property
    def as_list(self):
        return [key for key in self.flags if getattr(self, key)]


class DataFlagAttributes(FlagAttributes):
    flags = {
        "has_target_id_list": (0, 0x01),
        "has_link_info": (0, 0x02),
        "has_description": (0, 0x04),
        "has_relative_path": (0, 0x08),
        "has_working_directory": (0, 0x10),
        "has_arguments": (0, 0x20),
        "has_icon_location": (0, 0x40),
        "is_unicode": (0, 0x80),
        "force_no_link_info": (1, 0x01),
        "has_exp_str": (1, 0x02),
        "run_in_separate_process": (1, 0x04),
        "has_darwin_id": (1, 0x10),
        "run_as_user": (1, 0x20),
        "has_exp_icon": (1, 0x40),
        "no_pidl_alias": (1, 0x80),
        "run_with_shim_layer": (2, 0x02),
        "force_no_link_track": (2, 0x04),
        "enable_target_metadata": (2, 0x08),
        "disable_link_path_tracking": (2, 0x10),
        "disable_known_folder_tracking": (2, 0x20),
        "disable_known_folder_alias": (2, 0x40),
        "allow_link_to_link": (2, 0x80),
        "unalias_on_save": (3, 0x01),
        "prefer_environment_path": (3, 0x02),
        "keep_local_id_list_for_unc_target": (3, 0x04),
    }

    def get_data_string_order(self):
        data_strings = []
        if self.has_description:
            data_strings.append("description")
        if self.has_relative_path:
            data_strings.append("relative_path")
        if self.has_working_directory:
            data_strings.append("working_directory")
        if self.has_arguments:
            data_strings.append("command_line_arguments")
        if self.has_icon_location:
            data_strings.append("icon_location")
        return data_strings


class FileFlagAttributes(FlagAttributes):
    flags = {
        "read_only" : (0, 0x01),
        "hidden": (0, 0x02),
        "system": (0, 0x04),
        "directory": (0, 0x10),
        "archive": (0, 0x20),
        "ntfs_efs": (0, 0x40),
        "normal": (0, 0x80),
        "temporary": (1, 0x01),
        "sparse": (1, 0x02),
        "reparse_point_data": (1, 0x04),
        "compressed": (1, 0x08),
        "offline": (1, 0x10),
        "attribute_not_content_indexed": (1, 0x20),
        "encrypted": (1, 0x40),
        "virtual": (2, 0x01)
    }


class LocationFlagAttributes(FlagAttributes):
    flags = {
        "local_volume": (0, 0x01),
        "network_share": (0, 0x02)
    }


class NetworkShareFlags(FlagAttributes):
    flags = {
        "valid_device": (0, 1),
        "valid_net_type": (0, 2)
    }


class LocationInformationHeader(DictReprMixin):
    def __init__(self, data_size, header_size, flags, volume_info_offset,
                 local_path_offset, network_shar_offset, common_path_offset,
                 unicode_local_path_offset, unicode_common_path_offset):
        self.data_size = data_size
        self.header_size = header_size
        self.volume_flags = LocationFlagAttributes(flags)
        self.volume_info_offset = volume_info_offset
        self.local_path_offset = local_path_offset
        self.network_share_offset = network_shar_offset
        self.common_path_offset = common_path_offset
        self.unicode_local_path_offset = unicode_local_path_offset if self.header_size > 28 else None
        self.unicode_common_path_offset = unicode_common_path_offset if self.header_size > 32 else None

    def get_common_path(self, location_data):
        if self.unicode_common_path_offset:
            return location_data[self.unicode_common_path_offset:].decode('utf-16le').strip('\x00')
        elif self.common_path_offset:
            return location_data[self.common_path_offset:].decode('utf-8').strip('\x00')
        return None

    def get_local_path(self, location_data):
        if self.unicode_local_path_offset:
            return location_data[self.unicode_local_path_offset:self.common_path_offset].decode(
                'utf-16le').strip('\x00')
        elif self.local_path_offset:
            return location_data[self.local_path_offset:self.common_path_offset].decode('utf-8').strip('\x00')
        return None


class NetworkShare(DictReprMixin):
    NETWORK_PROVIDER = {
        0x001a0000: "WNNC_NET_AVID",
        0x001b0000: "WNNC_NET_DOCUSPACE",
        0x001c0000: "WNNC_NET_MANGOSOFT",
        0x001d0000: "WNNC_NET_SERNET",
        0x001e0000: "WNNC_NET_RIVERFRONT1",
        0x001f0000: "WNNC_NET_RIVERFRONT2",
        0x00200000: "WNNC_NET_DECORB",
        0x00210000: "WNNC_NET_PROTSTOR",
        0x00220000: "WNNC_NET_FJ_REDIR",
        0x00230000: "WNNC_NET_DISTINCT",
        0x00240000: "WNNC_NET_TWINS",
        0x00250000: "WNNC_NET_RDR2SAMPLE",
        0x00260000: "WNNC_NET_CSC",
        0x00270000: "WNNC_NET_3IN1",
        0x00290000: "WNNC_NET_EXTENDNET",
        0x002a0000: "WNNC_NET_STAC",
        0x002b0000: "WNNC_NET_FOXBAT",
        0x002c0000: "WNNC_NET_YAHOO",
        0x002d0000: "WNNC_NET_EXIFS",
        0x002e0000: "WNNC_NET_DAV",
        0x002f0000: "WNNC_NET_KNOWARE",
        0x00300000: "WNNC_NET_OBJECT_DIRE",
        0x00310000: "WNNC_NET_MASFAX",
        0x00320000: "WNNC_NET_HOB_NFS",
        0x00330000: "WNNC_NET_SHIVA",
        0x00340000: "WNNC_NET_IBMAL",
        0x00350000: "WNNC_NET_LOCK",
        0x00360000: "WNNC_NET_TERMSRV",
        0x00370000: "WNNC_NET_SRT",
        0x00380000: "WNNC_NET_QUINCY",
        0x00390000: "WNNC_NET_OPENAFS",
        0x003a0000: "WNNC_NET_AVID1",
        0x003b0000: "WNNC_NET_DFS",
        0x003c0000: "WNNC_NET_KWNP",
        0x003d0000: "WNNC_NET_ZENWORKS",
        0x003f0000: "WNNC_NET_VMWARE",
        0x00400000: "WNNC_NET_RSFX",
        0x00410000: "WNNC_NET_MFILES",
        0x00420000: "WNNC_NET_MS_NFS",
        0x00430000: "WNNC_NET_GOOGLE"
    }

    def __init__(self, size, flags, offset_to_share_name, offset_to_device_name, network_provider_type, data):
        self.size = size
        self.flags = NetworkShareFlags(flags)
        self.offset_to_share_name = offset_to_share_name
        self.offset_to_device_name = offset_to_device_name
        self.network_provider = self.NETWORK_PROVIDER[network_provider_type]
        if self.offset_to_share_name > 20:
            self.unicode_offset_to_share_name, self.unicode_offset_to_device_name = struct.unpack(
                '<2I', data[:8]
            )
            share_name_data_offset = self.unicode_offset_to_share_name - 20
            device_name_data_offset = self.unicode_offset_to_device_name - 20
            end_offset = self.size - 20
            share_data = data[share_name_data_offset:device_name_data_offset]
            self.network_share_name = share_data.decode('utf-16le').strip('\x00')
            self.device_name = data[device_name_data_offset:end_offset].decode('utf-16le').strip('\x00')
        else:
            self.unicode_offset_to_device_name = None
            self.unicode_offset_to_share_name = None
            share_name_data_offset = self.offset_to_share_name - 20
            device_name_data_offset = self.offset_to_device_name - 20
            end_offset = self.size - 20
            share_data = data[share_name_data_offset:device_name_data_offset]
            self.network_share_name = share_data.decode('utf-8').strip('\x00')
            self.device_name = data[device_name_data_offset:end_offset].decode('utf-8').strip('\x00')


class LocalVolumeHeader(DictReprMixin):
    DRIVE_TYPES = [
        "DRIVE_UNKNOWN",
        "DRIVE_NO_ROOT_DIR",
        "DRIVE_REMOVABLE",
        "DRIVE_FIXED",
        "DRIVE_REMOTE",
        "DRIVE_CDROM",
        "DRIVE_RAMDISK"
    ]

    def __init__(self, size, drive_type, drive_serial_number, offset, data):
        self.size = size
        try:
            self.drive_type = self.DRIVE_TYPES[drive_type]
        except IndexError:
            self.drive_type = "ERROR BAD DRIVE TYPE: {}".format(drive_type)
        self.serial_number = binascii.hexlify(drive_serial_number[::-1])
        self.offset = offset
        data_end = self.size - 16
        if self.offset > 16:
            self.unicode_offset, = struct.unpack('<I', data[:4])
            data_offset = self.unicode_offset - 16
            self.volume_label = data[data_offset:data_end].decode('utf-16le').strip('\x00')
        else:
            self.unicode_offset = None
            self.volume_label = data[:data_end].decode('utf-8').strip('\x00')


class LnkFileHeader(DictReprMixin):
    _show_win_values = {
        0: "SW_HIDE",
        1: "SW_NORMAL",
        2: "SW_SHOWMINIMIZED",
        3: "SW_SHOWMAXIMIZED",
        4: "SW_SHOWNOACTIVE",
        5: "SW_SHOW",
        6: "SW_MINIMIZE",
        7: "SW_SHOWMINNOACTIVE",
        8: "SW_SHOWNA",
        9: "SW_RESTORE",
        10: "SW_SHOWDEFAULT",
        11: "SW_FORCEMINIMIZE"
    }

    def __init__(self, size, guid, data_flags,
                 file_attribute_flags, creation_filetime, last_access_filetime,
                 last_modification_filetime, file_size, icon_index, show_window,
                 hot_key, *args):
        self.header_size = size
        self.guid = guid
        self.link_flags = DataFlagAttributes(data_flags)
        self.file_attributes = FileFlagAttributes(file_attribute_flags)
        self.created = ms_time_to_unix(creation_filetime)
        self.last_accessed = ms_time_to_unix(last_access_filetime)
        self.last_modified = ms_time_to_unix(last_modification_filetime)
        self.file_size = file_size
        self.icon_index = icon_index
        self.show_window = self._show_win_values[show_window]
        self.hot_key = hot_key


class LnkParser:
    HEADER_STRUCT_FORMAT = '<i16s4s4sQQQIiIH10s'
    LOCATION_HEADER_FORMAT = '<2I4s6I'
    LOCAL_VOLUME_FORMAT = '<II4sI{}s'
    NETWORK_SHARE_FORMAT = '<I4sIII{}s'

    FILE_HEADER_SIZE = 76
    LIST_TARGET_IDENTIFIER_SIZE_BYTES = 2
    # location header can actually be 28, 32, or 36 long.. we take the longest and handle it afterwards.
    LOCATION_HEADER_SIZE = 36

    def __init__(self, data):
        self.data = data
        self._location_offset = None
        self._link_header = None
        self._location_header = None
        self._local_volume_header = None
        self._network_share = None
        self._local_path = None
        self._common_path = None
        self._location_data = None
        self._data_strings = None
        self._target_id_list_size = None
        self.assert_signature()

    def assert_signature(self):
        header_size, guid = struct.unpack('<I16s', self.data[:20])
        if header_size != 76:
            raise LnkParsingException("This is not a .lnk file.")
        if guid != b'\x01\x14\x02\x00\x00\x00\x00\x00\xc0\x00\x00\x00\x00\x00\x00F':
            raise LnkParsingException("Cannot read this kind of .lnk file.")

    @property
    def link_header(self):
        """
        The link header contains information that is in every lnk file.
        It has some meta fields, but it also tells us via the link_flags
        which other sections the file contains.

        :return: a LnkFileHeader that is used to determine what else is in the file
        """
        if self._link_header is None:
            self._link_header = LnkFileHeader(*struct.unpack(self.HEADER_STRUCT_FORMAT, self.link_header_data()))
        return self._link_header

    def link_header_data(self):
        return self.data[:self.FILE_HEADER_SIZE]

    ########################################################
    # Offsets to  each section that might be in the file
    #
    # The order of sections of the file goes:
    #   1) Link Header
    #   2) TargetIdList - if has_target_id_list
    #   3) Location Information - if has_link_info
    #   4) Data Strings - the strings expected are specified in the link_flags
    #   5) Data Block - binary data blocks, not yet supported.

    @property
    def target_id_list_offset(self):
        return self.FILE_HEADER_SIZE

    @property
    def target_id_list_size(self):
        if self._target_id_list_size is None:
            if self.link_header.link_flags.has_target_id_list:
                lti_size_data = self.data[self.FILE_HEADER_SIZE:self.FILE_HEADER_SIZE+self.LIST_TARGET_IDENTIFIER_SIZE_BYTES]
                self._target_id_list_size, = struct.unpack('<H', lti_size_data)
                # we need to account for the first two bytes we also removed
                self._target_id_list_size += self.LIST_TARGET_IDENTIFIER_SIZE_BYTES
            else:
                self._target_id_list_size = 0
        return self._target_id_list_size

    @property
    def location_offset(self):
        if self._location_offset is None:
            self._location_offset = self.target_id_list_offset + self.target_id_list_size
        return self._location_offset

    @property
    def location_size(self):
        if not self.link_header.link_flags.has_link_info:
            return 0
        return self.location_header.data_size

    @property
    def data_strings_offset(self):
        return self.location_offset + self.location_size


    ##############################
    # Location Information

    @property
    def location_header(self):
        if not self.link_header.link_flags.has_link_info:
            return None

        if self._location_header is None:
            self._location_header = LocationInformationHeader(
                *struct.unpack(self.LOCATION_HEADER_FORMAT, self.get_location_header_data())
            )
        return self._location_header

    def get_location_header_data(self):
        """
        Helper function, only valid if has_lin_info is true
        """
        offset = self.location_offset
        end_offset = offset + self.LOCATION_HEADER_SIZE
        return self.data[offset:end_offset]

    def has_local_volume(self):
        return self.location_header and \
               self.location_header.volume_flags.local_volume and \
               self.location_header.volume_info_offset > 0

    @property
    def local_volume_header(self):
        if self.has_local_volume() and self._local_volume_header is None:
            offset = self.location_offset + self.location_header.volume_info_offset
            end_offset = offset + self.location_header.data_size
            extra_data_size = self.location_header.data_size - 16
            local_volume_format = self.LOCAL_VOLUME_FORMAT.format(extra_data_size)
            self._local_volume_header = LocalVolumeHeader(
                *struct.unpack(
                    local_volume_format, self.data[offset:end_offset]
                )
            )
        return self._local_volume_header

    def has_local_path(self):
        return self.location_header and self.location_header.local_path_offset > 0

    def has_common_path(self):
        return self.location_header and self.location_header.common_path_offset > 0

    @property
    def location_data(self):
        if self._location_data is None:
            end_offset = self.location_offset + self.location_size
            self._location_data = self.data[self.location_offset:end_offset]

        return self._location_data

    @property
    def local_path(self):
        if self.has_local_path() and self._local_path is None:
            self._local_path = self.location_header.get_local_path(self.location_data)
        return self._local_path

    @property
    def common_path(self):
        if self.has_common_path() and self._common_path is None:
            self._common_path = self.location_header.get_common_path(self.location_data)
        return self._common_path

    def has_network_share(self):
        return self.location_header and \
               self.location_header.volume_flags.network_share and \
               self.location_header.network_share_offset > 0

    @property
    def network_share(self):
        if self.has_network_share() and self._network_share is None:
            offset = self.location_offset + self.location_header.network_share_offset
            end_offset = offset + self.location_header.data_size
            extra_data_size = self.location_header.data_size - 20
            network_share_format = self.NETWORK_SHARE_FORMAT.format(extra_data_size)
            self._network_share = NetworkShare(
                *struct.unpack(network_share_format, self.data[offset:end_offset])
            )
        return self._network_share

    @property
    def data_strings(self):
        if self._data_strings is None:
            if self.link_header.link_flags.is_unicode:
                my_codec = 'utf-16le'
                char_size = 2
            else:
                my_codec = 'utf-8'
                char_size = 1
            ds_data = self.data[self.data_strings_offset:]
            self._data_strings = {}
            for data_string in self.link_header.link_flags.get_data_string_order():
                ds_size, = struct.unpack('<H', ds_data[:2])
                ds_size *= char_size
                self._data_strings[data_string] = ds_data[2:2+ds_size].decode(my_codec).strip('\x00')
                ds_data = ds_data[2+ds_size:]

        return self._data_strings

    @classmethod
    def init_from_filename(cls, filename):
        with open(filename, 'rb') as f:
            data = f.read()
        return cls(data)

    @classmethod
    def init_from_file_handle(cls, file):
        data = file.read()
        return cls(data)
