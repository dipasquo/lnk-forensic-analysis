from django.apps import AppConfig


class FileForensicsConfig(AppConfig):
    name = 'file_forensics'
