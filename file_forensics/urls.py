from django.conf.urls import url, include
from rest_framework import routers
from file_forensics.views import ForensicFileViewSet

router = routers.DefaultRouter()
router.register(r'forensic-files', ForensicFileViewSet)

urlpatterns = [
    url(r'^api/', include(router.urls))
]