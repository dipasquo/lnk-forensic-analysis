from django.core.files.storage import default_storage
from django.http import HttpResponse
from django.views.generic import TemplateView
from rest_framework import viewsets, mixins
from rest_framework.exceptions import ValidationError
from rest_framework.permissions import AllowAny
from rest_framework.renderers import JSONRenderer

from file_forensics.models import ForensicFileModel
from file_forensics.parsers.lnk_parser import LnkParser, LnkParsingException
from file_forensics.serializers import ForensicFileSerializer, LnkFileSerializer


class ForensicFileViewSet(mixins.CreateModelMixin, mixins.RetrieveModelMixin,
                          mixins.ListModelMixin, viewsets.GenericViewSet):
    queryset = ForensicFileModel.objects.all()
    serializer_class = ForensicFileSerializer
    permission_classes = (AllowAny,)

    def perform_create(self, serializer):
        f = self.request.data.get('file')

        if f.size > ForensicFileModel.MAX_FILE_SIZE:
            raise ValidationError("Maximum file size is {} bytes.".format(ForensicFileModel.MAX_FILE_SIZE))

        file_data = f.read()
        file_name = f.name
        f.seek(0)

        try:
            parser = LnkParser(file_data)
            analysis_serializer = LnkFileSerializer(parser)
            analysis = analysis_serializer.data
            file_type = ForensicFileModel.TYPE_LNK
        except Exception as ex:
            file_type = ForensicFileModel.TYPE_UNKNOWN
            analysis = {"errorMessage": str(ex)}

        serializer.save(
            file=f,
            file_name=file_name,
            analysis=analysis,
            type=file_type)


class IndexView(TemplateView):
    template_name = 'file_forensics/file-explorer.html'

    def get_context_data(self, **kwargs):
        context = kwargs
        recent_files = ForensicFileModel.objects.all().order_by('-id')[:20]
        serializer = ForensicFileSerializer(recent_files, many=True)
        renderer = JSONRenderer()
        context["recent_files"] = renderer.render(serializer.data)
        return context


def ping(request):
    return HttpResponse("I'm alive!")