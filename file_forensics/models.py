from django.db import models
from django.utils import timezone

from file_forensics.fields import EncodedJSONField
from file_forensics.parsers.lnk_parser import LnkParser


def update_filename(instance, filename):
    return "forensics-files/%s-%s" % (timezone.now().isoformat(),
                                          filename.replace(' ', '').replace('.', '')[:10])


class ForensicFileModel(models.Model):
    MAX_FILE_SIZE = 100000
    file = models.FileField(upload_to=update_filename)
    file_name = models.CharField(max_length=256)
    uploaded_at = models.DateTimeField(auto_now_add=True)

    TYPE_UNKNOWN = -1
    TYPE_LNK = 1

    FILE_TYPE_CHOICES = (
        (TYPE_UNKNOWN, "Unrecognized"),
        (TYPE_LNK, "lnk"),
    )

    type = models.IntegerField(choices=FILE_TYPE_CHOICES, default=TYPE_LNK)
    analysis = EncodedJSONField(default=dict)

    def get_link_parser(self):
        self.file.open(mode='rb')
        data = self.file.file.read()
        self.file.close()
        return LnkParser(data)
