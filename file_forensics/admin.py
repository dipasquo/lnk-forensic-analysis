from django.contrib import admin

from file_forensics.models import ForensicFileModel


@admin.register(ForensicFileModel)
class ForensicFileAdmin(admin.ModelAdmin):
    list_display = ('uploaded_at', 'file_name', 'type', 'file')
    list_filter = ('type',)
