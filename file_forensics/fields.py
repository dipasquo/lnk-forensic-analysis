import json

from django.contrib.postgres.fields import JSONField
from psycopg2.extras import Json
from rest_framework.utils import encoders


class EncodedJSONField(JSONField):
    encoder_class = encoders.JSONEncoder

    def get_prep_value(self, value):
        if value is not None:
            return Json(value, dumps=lambda x: json.dumps(x, cls=self.encoder_class))
        return value
