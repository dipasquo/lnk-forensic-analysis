from rest_framework import serializers

from file_forensics.models import ForensicFileModel


class LocalVolumeSerializer(serializers.Serializer):
    drive_type = serializers.CharField(allow_blank=True)
    serial_number = serializers.CharField(allow_blank=True)
    volume_label = serializers.CharField(allow_blank=True)


class NetworkShareSerializer(serializers.Serializer):
    network_provider = serializers.CharField()
    network_share_name = serializers.CharField(allow_blank=True)
    device_name = serializers.CharField(allow_blank=True)


class LnkFileSerializer(serializers.Serializer):
    file_size = serializers.IntegerField(source='link_header.file_size')
    date_created = serializers.DateTimeField(source='link_header.created')
    date_modified = serializers.DateTimeField(source='link_header.last_modified')
    last_accessed = serializers.DateTimeField(source='link_header.last_accessed')
    file_attributes = serializers.ListField(source='link_header.file_attributes.as_list',
                                            child=serializers.CharField())
    link_flags = serializers.ListField(source='link_header.link_flags.as_list', child=serializers.CharField())
    show_window = serializers.CharField(source='link_header.show_window')
    hot_key = serializers.IntegerField(source='link_header.hot_key')
    icon_index = serializers.IntegerField(source='link_header.icon_index')
    has_location_info = serializers.BooleanField(source='link_header.link_flags.has_link_info')
    local_volume = LocalVolumeSerializer(source='local_volume_header', allow_null=True)
    network_share = NetworkShareSerializer(allow_null=True)
    local_path = serializers.CharField(allow_null=True, allow_blank=True)
    common_path = serializers.CharField(allow_null=True, allow_blank=True)
    data_strings = serializers.JSONField()


class ForensicFileSerializer(serializers.ModelSerializer):
    type = serializers.IntegerField(required=False)
    analysis = serializers.JSONField(required=False)
    file_name = serializers.CharField(required=False)

    class Meta:
        model = ForensicFileModel
        fields = ('id', 'file', 'file_name', 'type', 'uploaded_at',
                  'analysis')
