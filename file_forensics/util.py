from datetime import datetime


def ms_time_to_unix(windows_time):
    unix_time = windows_time / 10000000.0 - 11644473600
    return datetime.fromtimestamp(unix_time)