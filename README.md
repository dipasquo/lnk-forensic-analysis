# README #

## DigiTrust Development Challenge - LNK Forensic Analysis Application ##
 
Objectives:

* Create a robust web application that enables forensic researchers to upload suspected Windows LNK files and receive a visual rendering of their relevant data.
* Utilize open source code as needed.
* Enable remote web access to the application
* Grant remote access to the application's source code in a password-protected GitHub repository. Other candidates will be taking this challenge as well so please be careful to not publicly release this code.
 
Technology stack:

* Python for server-side application
* Flask, Django or equivalent for web server
* Angular for user interface
* Cloud-based server hosting such as AWS or Google Cloud (free tier)
 
References:

* https://www.magnetforensics.com/computer-forensics/forensic-analysis-of-lnk-files/
* https://github.com/HarmJ0y/pylnker/blob/master/pylnker.py

