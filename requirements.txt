boto==2.45.0
Django==1.10.5
django-storages==1.5.2
djangorestframework==3.5.3
gunicorn==19.6.0
psycopg2==2.6.2
