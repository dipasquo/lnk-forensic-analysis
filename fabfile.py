import json
import re

import requests
from subprocess import call, Popen, PIPE
from fabric.api import run
from fabric.context_managers import cd
from fabric.decorators import roles, runs_once, task
from fabric.operations import sudo
from fabric.state import env
from fabric.tasks import execute

env.use_ssh_config = True

env.roledefs = {
    'production': ['digi']
}


@task
def deploy_a_machine(deploy_to, from_branch):
    #get the latest and greatest from git
    with cd('/home/ubuntu/lnk-forensic-analysis'):
        run('git fetch -a')
        run('git reset --hard')
        run('git checkout {}'.format(from_branch))
        run('git pull --no-edit origin {}'.format(from_branch))

        # install pip requirments
        sudo('pip3 install -r requirements.txt')

        # make sure the correct settings files are being used
        sudo('mkdir -p /etc/lnktest')
        sudo('chmod a+r /etc/lnktest')
        sudo('ln -svf /home/ubuntu/lnk-forensic-analysis/conf/{}-settings.ini /etc/lnktest/settings.ini'.format(deploy_to))

        # use the latest and greatest nginx configuration
        sudo('rm -f /etc/nginx/sites-enabled/default')
        sudo('ln -svf /home/ubuntu/lnk-forensic-analysis/etc/nginx/sites-enabled/default /etc/nginx/sites-enabled/default')

        # use the latest supervisor configuration
        sudo('cp -f /home/ubuntu/lnk-forensic-analysis/etc/supervisor/conf.d/gunicorn_django.conf /etc/supervisor/conf.d/gunicorn_django.conf')

        #reread the config with supervisor
        sudo('supervisorctl reread')

        sudo('./manage.py migrate --noinput')
        sudo('./manage.py collectstatic --noinput')

        #restart the app
        # ugg supervisor is having issues.. don't have time to fix this
        #sudo('sudo service supervisor restart')
        # just going to run gunicorn in daemon mode.. and (so bad) kill all python 3 process
        # to restart.. obviously, would never do this in a real production environment.. would create
        # a service either in /etc/init or supervisor

        #restart nginx
        sudo('service nginx restart')

        #sleep to let django startup
        run('sleep 10')


@task
def deploy(deploy_to, from_branch="master"):
    env.hosts = env.roledefs[deploy_to]
    execute(deploy_a_machine, deploy_to, from_branch)

