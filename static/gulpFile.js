// Include gulp
var gulp = require('gulp');

// Include Our Plugins
var plumber = require('gulp-plumber'),
    sass = require('gulp-ruby-sass'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify'),
    minifyCSS = require('gulp-minify-css'),
    rename = require('gulp-rename'),
    sourcemaps = require('gulp-sourcemaps'),
    karma;

/**
 * Run test once and exit
 */
gulp.task('test', function (done) {
    karma = require('karma').Server;
    karma.start({
        configFile: __dirname + '/karma.config.js',
        singleRun: true
    }, function () {
        done();
    });
});


// Concatenate & Minify JS
gulp.task('js-staging', function () {
    return gulp.src(['src/app/**/*.js', 'src/app/app.js'])
        .pipe(plumber())
        .pipe(sourcemaps.init())
        .pipe(concat('build.js'))
        //.pipe(uglify({mangle: false}))
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest('dist/js'));
});

gulp.task('js-production', function () {
    return gulp.src(['src/app/**/*.js', 'src/app/app.js'])
        .pipe(plumber())
        //.pipe(sourcemaps.init())
        .pipe(concat('build.js'))
        .pipe(uglify({mangle: false}))
        //.pipe(sourcemaps.write('./'))
        .pipe(gulp.dest('dist/js'));
});

//Compile Sass and minify Main Site CSS
gulp.task('scss-main', function () {
    return sass('src/scss/main.scss', {
        'sourcemap=none': true,
        'compass': true,
        'style': 'compressed'
    }).on('error', function (err) {
            console.log(err.message);
        })
        .pipe(gulp.dest('dist/css'));
});


// Watch Files For Changes
gulp.task('watch', function () {
    gulp.watch('src/app/**/*.js', ['js']);
});


// One off task for building for deployment
gulp.task('build', ['scss-main', 'scss-login', 'js-production']);
gulp.task('build-staging', ['scss-main', 'scss-login', 'js-staging']);
gulp.task('js', ['scss-main','js-staging']);

// Default Task
gulp.task('default', ['scss-main', 'js-staging', 'watch']);
