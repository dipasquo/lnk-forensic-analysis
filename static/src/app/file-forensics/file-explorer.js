(function (angular, undefined) {
    var app = angular.module('digiTrustForensicsApp.fileExplore', ['ngFileUpload']);

    app.controller('FileExplorerCtrl', ['Upload', '$window', '$timeout', function(Upload, $window, $timeout){
        var vm = this;
        vm.recentFiles = $window.digiTrust.recentFiles;
        vm.currentFile = null;
        vm.fileLoading = false;
        vm.file = null;
        vm.uploadFile = uploadFile;
        vm.openFile = openFile;


        function showFileDetailModal(){
            angular.element('#file-explorer-modal').modal('show');
        }

        function openFile(fileToOpen){
            vm.currentFile = fileToOpen;
            showFileDetailModal();
        }

        function uploadFile() {
            var csrftoken = getCookie('csrftoken');
            vm.file.upload = Upload.upload({
                url: '/forensics/api/forensic-files/',
                data: {file: vm.file},
                headers: {"X-CSRFToken": csrftoken}
            });
            vm.fileLoading = true;
            vm.errorMsg = null;
            vm.currentFile = null;
            showFileDetailModal();

            vm.file.upload.then(function (response) {
                $timeout(function () {
                    vm.currentFile = response.data;
                    vm.recentFiles.unshift(vm.currentFile);
                    vm.file = null;
                    vm.fileLoading = false;
                });
            }, function (response) {
                if (response.status > 0)
                    vm.errorMsg = response.status + ': ' + response.data;
                    vm.fileLoading = false;

            }, function (evt) {
                // Math.min is to fix IE which reports 200% sometimes
                vm.file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
            });
        }

        function getCookie(name) {
            var cookieValue = null;
            if (document.cookie && document.cookie !== '') {
                var cookies = document.cookie.split(';');
                for (var i = 0; i < cookies.length; i++) {
                    var cookie = jQuery.trim(cookies[i]);
                    // Does this cookie string begin with the name we want?
                    if (cookie.substring(0, name.length + 1) === (name + '=')) {
                        cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                        break;
                    }
                }
            }
            return cookieValue;
        }




    }]);


})(window.angular, void 0);