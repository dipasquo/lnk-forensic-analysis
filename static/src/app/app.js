!function (angular, undefined) {
    angular.module('digiTrustForensicsApp',
        ['digiTrustForensicsApp.fileExplore']
    );
}(angular, void 0);